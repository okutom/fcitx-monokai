#!/bin/sh

if ! type fcitx > /dev/null 2>&1; then
    echo fcitx is not installed
    exit
fi

script_dir=`dirname ${0}`
abs_script_dir=`cd ${script_dir}; pwd`

skin_dir=~/.config/fcitx/skin
def_skin_dir=/usr/share/fcitx/skin

mkdir -p ${skin_dir}
ln -snfv ${abs_script_dir}/monokai ${skin_dir}/monokai

cp ${def_skin_dir}/default/active.png ${skin_dir}/monokai/
cp ${def_skin_dir}/default/inactive.png ${skin_dir}/monokai/
cp ${def_skin_dir}/dark/cn.png ${skin_dir}/monokai/
cp ${def_skin_dir}/dark/en.png ${skin_dir}/monokai/
cp ${def_skin_dir}/dark/keyboard.png ${skin_dir}/monokai/
cp ${def_skin_dir}/dark/logo.png ${skin_dir}/monokai/

unset def_skin_dir
unset skin_dir
unset abs_script_dir
unset script_dir

